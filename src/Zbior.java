import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;

/**
 * Created by pacio on 28.11.2016.
 */
public class Zbior {
    private int[] tab;

    public Zbior(int tab[]){
        this.tab=tab;
    }


    public void wypisz(){
        for(int i=0; i<tab.length;i++){
            System.out.println(tab[i]);
        }
    }

    public boolean czyZawiera(int element){
        for(int i=0; i<=tab.length;i++){
            if(element==tab[i]){
                return true;
            }
        }
        return false;
    }

    public void dodaj(int nowy){
        if(!czyZawiera(nowy)){
            tab[rozmiar()]=nowy;
        }
    }

    public int rozmiar(){
       return tab.length;
    }

    public void wczytaj(String filename){

        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            String wiad;
            StringBuilder sb=new StringBuilder();
            wiad=br.readLine();
            while(wiad!=null){
                sb.append(wiad);
                sb.append(System.lineSeparator());
                wiad=br.readLine();

            }
            br.close();
            System.out.println("Rozmiar buforu po czytaniu: "+sb.length());
            wiad=sb.toString();
            System.out.print(wiad);

        }catch(Exception e){
            System.err.println("Błąd: "+e);
            e.printStackTrace();
        }

    }

    public void usun(int element){
        int tmp=0;
        for(int i=0;i<tab.length;i++){
            if(tab[i]==element){
                tmp=i;
            }
        }
        for(int j=tmp;j<tab.length-1;j++){
            tab[j]=tab[j+1];
        }
    }

}
